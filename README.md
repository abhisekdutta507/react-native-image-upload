Visit to learn more about [signed-apk-android](https://reactnative.dev/docs/signed-apk-android)

Download the [release apk](https://bitbucket.org/abhisekdutta507/react-native-image-upload/src/master/android/app/release/)

```bash
keytool -genkey -v -keystore my-upload-key.keystore -alias my-key-alias -keyalg RSA -keysize 2048 -validity 10000
```

```bash
keytool -importkeystore -srckeystore my-upload-key.keystore -destkeystore my-upload-key.keystore -deststoretype pkcs12
```


