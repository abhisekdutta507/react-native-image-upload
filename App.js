import { StatusBar } from 'expo-status-bar'
import React, { useState } from 'react'
import { StyleSheet, SafeAreaView, TouchableOpacity, Text, View, Image, ActivityIndicator, Alert, Linking } from 'react-native'
import { launchImageLibrary } from 'react-native-image-picker'

export default function App () {
  const [state, setState] = useState({
    defaultImageUrl: require('./assets/image-gallery.png'),
    uploading: false
  })

  /**
   * @description open the file drawer to choose the file
   */
  const chooseImage = () => {
    // wait for the upload to be completed
    if (state.uploading) { return }

    // More info on all the options is below in the API Reference... just some common use cases shown here
    const options = {
      quality: 1,
      includeBase64: false,
    }

    launchImageLibrary(options, (response) => {
      if (response.didCancel) {
        //
      } else if (response.error) {
        //
      } else if (response.customButton) {
        //
      } else {
        setState({
          ...state,
          uploading: true
        })
        uploadImage(response)
      }
    })
  }

  /**
   * @description upload the image file
   */
  const uploadImage = async ({ uri, type, fileName }) => {
    const photo = {
      uri,
      type,
      name: fileName,
    }

    const form = new FormData()
    form.append('media', photo)

    let response
    try {
      response = await fetch('https://s3-file-manager.herokuapp.com/api/upload', {
        method: 'POST',
        body: form
      }).then((d) => d.json())
    } catch(error) {
      // show alert message
      Alert.alert(
        'ERROR',
        error.message,
        [
          { text: 'OK' }
        ],
        { cancelable: true }
      )
      
      // hide the loader
      setState({
        ...state,
        uploading: false
      })

      // exit from code block
      return
    }

    setState({
      ...state,
      uploading: false
    })

    const supported = await Linking.canOpenURL(response[0].Location)

    if (supported) {
      // Opening the link with some app, if the URL scheme is "http" the web link should be opened
      // by some browser in the mobile
      await Linking.openURL(response[0].Location)
    } else {
      // show alert message
      Alert.alert(
        'UPLOADED',
        `Don't know how to open this URL: ${response[0].Location}`,
        [
          { text: 'OK' }
        ],
        { cancelable: false }
      )
    }
  }

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar style="auto" />

      <View style={styles.screen}>
        <View style={{ ...styles.padding }}>
          <Text style={styles.heading}>Upload image here</Text>
        </View>

        <View style={{ ...styles.center, ...styles.padding }}>
          <Image source={state.defaultImageUrl} />
        </View>

        <View style={{ ...styles.padding }}>
          <TouchableOpacity style={styles.button} onPress={chooseImage}>
            {
              state.uploading
                ? <ActivityIndicator size="small" color="#ffffff" />
                : <Text>Choose Image</Text>
            }
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  screen: {
    flex: 1,
    width: '100%',
    paddingTop: 60,
    padding: 25
  },

  button: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: 40,
    backgroundColor: '#009688',
    borderRadius: 5,
  },

  heading: {
    fontSize: 20,
  },

  image: {
    width: '100%',
  },

  imageBackground: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center"
  },

  center: {
    alignItems: 'center'
  },

  padding: {
    paddingBottom: 30
  }
})
